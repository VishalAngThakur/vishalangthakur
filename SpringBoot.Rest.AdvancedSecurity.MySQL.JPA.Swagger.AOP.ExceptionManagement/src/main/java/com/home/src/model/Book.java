package com.home.src.model;

import javax.persistence.Entity;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.domain.AbstractPersistable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel(description="Understand Book model structure")
public class Book extends AbstractPersistable<Long> {

	@Size(min=5,message="isbn code should be atleast 5 character")
	@ApiModelProperty(notes="isbn code should be atleast 5 character")
	private String isbn;
	
	@Size(min=1,message="Name should have atleast 1 character")
	@ApiModelProperty(notes="Book name cannot be blank")
	private String bookName;
	private String author;
	private String publication;

	public Book() {
		super();
	}

	public Book(String isbn, String bookName, String author, String publication) {
		super();
		this.isbn = isbn;
		this.bookName = bookName;
		this.author = author;
		this.publication = publication;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	
	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublication() {
		return publication;
	}

	public void setPublication(String publication) {
		this.publication = publication;
	}

}
