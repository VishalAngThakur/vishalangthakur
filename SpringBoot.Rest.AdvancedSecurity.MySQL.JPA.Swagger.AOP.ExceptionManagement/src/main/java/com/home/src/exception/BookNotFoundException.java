package com.home.src.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class BookNotFoundException extends RuntimeException {
 
	private static final long serialVersionUID = 1L;

	public BookNotFoundException() {
		super("Book service not initialized, Try again later!!!");
	}

	public BookNotFoundException(String message) {
		super(message);
	}
	
	
   
}
