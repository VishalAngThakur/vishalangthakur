package com.home.src.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.home.src.dao.BookDAO;
import com.home.src.model.Book;

@Repository
@Transactional
public class BookDAOImpl implements BookDAO{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public boolean deleteArticle(String isbn) {
		String hql = "FROM Book where isbn=?  ";
		List<Book> lstBook = entityManager.createQuery(hql).setParameter(1, isbn).getResultList();
		if(null == lstBook || lstBook.isEmpty())
		{
			return false;
		}
		entityManager.remove(lstBook.get(0).getId());
		return true;
		
	}

	@Override
	public List<Book> lstBook(String author, String publication) {
		String hql = "FROM Book where author=? and publication=?";
		List<Book> lstBook = entityManager.createQuery(hql).setParameter(1, author)
				                                  .setParameter(2, publication).getResultList();
		return lstBook;
	}

}
