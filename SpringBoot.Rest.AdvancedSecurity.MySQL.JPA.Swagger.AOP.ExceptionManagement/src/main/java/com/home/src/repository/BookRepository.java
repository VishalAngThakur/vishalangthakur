package com.home.src.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.home.src.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

	@Query("FROM Book WHERE author=:author")
	List<Book> findBookByAuthor(@Param("author") String author);
	
	@Query("FROM Book WHERE publication=:publication")
	List<Book> getBookByPublication(@Param("publication") String publication);
	
	@Query("FROM Book WHERE isbn=:isbn")
	public Book getBookByISBN(@Param("isbn") String isbn);
	
	
	
}
