package com.home.src.dao;

import java.util.List;

import com.home.src.model.Book;

public interface BookDAO {
	boolean deleteArticle(String isbn);
    List<Book> lstBook(String title, String location);
}
