package com.home.src.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController implements ErrorController{
   private static final String PATH="/error";
   
   @Override
   public String getErrorPath() {
	   return PATH;
   }
   
   @RequestMapping(PATH)
   public String error()
   {
	   return "No mapping available";
   }
   
   @RequestMapping("/")
   public String welcome()
   {
	   return "Welcome to Home Library.../n/n Contact vishal.ang.thakur@gmail.com to identify all the end points!!!";
   }
   
}
