package com.home.src.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.home.src.exception.BookNotFoundException;
import com.home.src.exception.DuplicateBookException;
import com.home.src.model.Book;
import com.home.src.service.BookService;

@RestController
@RequestMapping("/books")
public class BookController {

	@Autowired
	BookService bookService;

	/*
	 * [TODO] Should be enhanced to implement pagination and find how to consume PagedResponse as a RestClient
	 */
	@GetMapping
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Book>> getAllBooks() {
		List<Book> lstBook = bookService.getAllBook();
		if (null == lstBook || lstBook.isEmpty()) {
			throw new BookNotFoundException();
		}
		return new ResponseEntity<>(lstBook, HttpStatus.OK);
	}

	@GetMapping("/id/{id}")
	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Book> getBookByID(@PathVariable Long id) {
		Book book = bookService.getBookByID(id);
		if (null == book) {
			throw new BookNotFoundException(" for ID " + id);
		}
		return new ResponseEntity<>(book, HttpStatus.FOUND);
	}

	@GetMapping("/isbn/{isbn}")
	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Book> getBookByISBN(@PathVariable String isbn) {
		Book book = bookService.getBookByISBN(isbn);
		if (null == book) {
			throw new BookNotFoundException(" for ISBN " + isbn);
		}
		return new ResponseEntity<>(book, HttpStatus.FOUND);
	}

	@GetMapping("/author/{authorName}")
	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Book>> getBookByAuthor(@PathVariable String authorName) {
		List<Book> lstBook = bookService.getBookByAuthor(authorName);
		if (null == lstBook || lstBook.isEmpty()) {
			throw new BookNotFoundException(" for author " + authorName);
		}
		return new ResponseEntity<>(lstBook, HttpStatus.FOUND);
	}

	@GetMapping("/publicationName/{publicationName}")
	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Book>> getBookByPublication(@PathVariable String publicationName) {
		List<Book> lstBook = bookService.getBookByPublication(publicationName);
		if (null == lstBook || lstBook.isEmpty()) {
			throw new BookNotFoundException("Book not found for publicationName " + publicationName);
		}
		return new ResponseEntity<>(lstBook, HttpStatus.FOUND);

	}

	@PostMapping
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<?> addBook(@RequestBody Book book, UriComponentsBuilder builder) {
		Book bookExists = bookService.getBookByISBN(book.getIsbn());
		if (null != bookExists) {
			throw new DuplicateBookException("Book with isbn: {" + book.getIsbn() + "} already exists");
		}

		bookService.createBook(book);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/isbn/{isbn}").buildAndExpand(book.getIsbn()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@PutMapping
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<?> updateArticle(@RequestBody Book book) {
		Book bookExist = bookService.getBookByISBN(book.getIsbn());
		if (null == bookExist) {
			throw new BookNotFoundException("Book with isbn: {" + bookExist.getIsbn() + "} not found");
		}
		bookService.updateBook(book);
		return new ResponseEntity<Book>(book, HttpStatus.OK);
	}
	

	@DeleteMapping("/isbn/{isbn}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<?> deleteBookByISBN(@PathVariable String isbn) {
		boolean deleted  = bookService.deleteBookByISBN(isbn);
		if(deleted)
			return new ResponseEntity<>(HttpStatus.OK);
		else
			throw new BookNotFoundException("Book with isbn: {" + isbn + "} not found");
	}

}
