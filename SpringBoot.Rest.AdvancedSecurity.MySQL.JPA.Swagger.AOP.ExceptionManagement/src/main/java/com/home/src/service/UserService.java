package com.home.src.service;

import java.util.List;

import com.home.src.model.User;


public interface UserService {
	List<User> userList();
	
	User findOne(Long id);
	
	User addUser(User user);
	
	String deleteUser(Long id);
}
