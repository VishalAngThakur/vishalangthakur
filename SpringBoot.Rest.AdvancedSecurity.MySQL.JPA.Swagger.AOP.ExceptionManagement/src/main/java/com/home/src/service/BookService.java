package com.home.src.service;

import java.util.List;

import com.home.src.model.Book;

public interface BookService {

	public List<Book> getAllBook();
	public Book getBookByID(Long id);
	public List<Book> getBookByAuthor(String authorName);
	public List<Book> getBookByPublication(String publicationName);
	public Book getBookByISBN(String isbn);
	public Book createBook(Book book);
	public Book updateBook(Book book);
	public boolean deleteBookByISBN(String isbn);
	public List<Book> bookBasedOnQuery(String authorName, String publicationName);
	
}
