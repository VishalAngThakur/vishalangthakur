package com.home.src.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.home.src.model.Book;
import com.home.src.repository.BookRepository;
import com.home.src.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	BookRepository bookRepository;
	
	@Override
	public List<Book> getAllBook() {
		return bookRepository.findAll();
	}

	@Override
	public Book getBookByID(Long id) {
		return bookRepository.findById(id).get();
	}

	@Override
	public List<Book> getBookByAuthor(String authorName) {
		return bookRepository.findBookByAuthor(authorName);
	}

	@Override
	public List<Book> getBookByPublication(String publicationName) {
		return bookRepository.getBookByPublication(publicationName);
	}

	@Override
	public Book getBookByISBN(String isbn) {
		return bookRepository.getBookByISBN(isbn);
	}

	@Override
	public Book createBook(Book book) {
		return bookRepository.save(book);
	}

	@Override
	public boolean deleteBookByISBN(String isbn) {
		Book book = bookRepository.getBookByISBN(isbn);
		if(null == book)
		{
			return false; 
		}
        bookRepository.deleteById(book.getId());
		return true;
	}

	@Override
	public Book updateBook(Book book) {
		return bookRepository.saveAndFlush(book);
	}

	@Override
	public List<Book> bookBasedOnQuery(String authorName, String publicationName) {
		// TODO Auto-generated method stub
		return null;
	}


}
