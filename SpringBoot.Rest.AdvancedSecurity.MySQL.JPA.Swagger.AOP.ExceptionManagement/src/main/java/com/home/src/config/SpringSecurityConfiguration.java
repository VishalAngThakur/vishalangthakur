package com.home.src.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.home.src.service.impl.MyUserDetailsService;


@Configuration
@EnableWebSecurity //This annotation enables the Spring security
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private AuthenticationEntryPoint entryPoint;

	@Autowired
	private MyUserDetailsService userDetailsService;

	/*
	 * Password encoder is needed from Spring 1.5/Springboot 2.0
	 * It encode/decodes the password passed as request 
     * Refer: https://www.youtube.com/watch?v=egXtoL5Kg08 [Very good example]
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(new PasswordEncoder() {
			
			@Override
			public boolean matches(CharSequence charSequence, String arg1) {
				return true;
			}
			
			@Override
			public String encode(CharSequence charSequence) {
				return charSequence.toString();
			}
		});
	}

	
	/*
	 * This method define which url path should be secured and which should not be
	 * Below every page is secured but we can have matcher to choose page as candidate for security
	 * http.authorizeRequests().anyMatchers("starstar/secured/starstar").authenticated
	 *  
    */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().anyRequest().authenticated().and().
		httpBasic().authenticationEntryPoint(entryPoint);
	}

	/*
	 * This method tells to authenticate request from user -> "user" and password-> "password"
	 * @Autowired public void configureGlobal(AuthenticationManagerBuilder auth)
	 * throws Exception {
	 * auth.inMemoryAuthentication().withUser("user").password("password").roles
	 * ("USER"); }
	 */

	/*
	 * @Override protected void configure(HttpSecurity http) throws Exception {
	 * http.authorizeRequests().anyRequest().authenticated().and().httpBasic();
	 * }
	 */

}
