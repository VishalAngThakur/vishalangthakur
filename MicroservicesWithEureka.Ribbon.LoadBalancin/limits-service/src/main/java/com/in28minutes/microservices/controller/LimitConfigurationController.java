package com.in28minutes.microservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.in28minutes.microservices.Configuration;
import com.in28minutes.microservices.model.LimitConfiguration;

@RestController
public class LimitConfigurationController {
	
	@Autowired
	private Configuration configuration;
   
	@GetMapping(path="/limits")  
	public LimitConfiguration retrieveLimit()
	{
		return new LimitConfiguration(configuration.getMaximum(), configuration.getMinimum());
	}
}
