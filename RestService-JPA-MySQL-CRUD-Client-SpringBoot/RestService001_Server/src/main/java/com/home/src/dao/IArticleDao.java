package com.home.src.dao;

import java.util.List;

import com.home.src.entity.Article;


public interface IArticleDao {
	List<Article> getAllArticles();
    Article getArticleById(int articleId);
    void addArticle(Article article);
    void updateArticle(Article article);
    void deleteArticle(int articleId);
    boolean articleExists(String title, String location);
}
