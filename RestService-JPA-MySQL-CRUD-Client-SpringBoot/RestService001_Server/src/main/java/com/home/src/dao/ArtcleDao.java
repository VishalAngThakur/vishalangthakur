package com.home.src.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.home.src.entity.Article;


@Repository   // @Repository is  a marker for any class that fulfills role of DAO class
@Transactional // [Vishal]- TO-DO
public class ArtcleDao implements IArticleDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Article> getAllArticles() {
		String hql = "FROM Article order by articleId";
		return entityManager.createQuery(hql).getResultList();
	}

	@Override
	public Article getArticleById(int articleId) {
		return entityManager.find(Article.class, articleId);
	}

	@Override
	public void addArticle(Article article) {
        entityManager.persist(article);
	}

	@Override
	public void updateArticle(Article article) {
		Article artcl = getArticleById(article.getArticleId());
        artcl.setTitle(article.getTitle());
        artcl.setCategory(article.getCategory());
        entityManager.flush();
        
        /*
       A call to EntityManager.flush(); will force the data to be persist in the database immediately as EntityManager.persist() will not (depending on how the EntityManager 
       is configured : FlushModeType (AUTO or COMMIT) by default it's set to AUTO and a flush  will be done automatically by if it's set to COMMIT the persitence of the data to the 
       underlying database will be delayed when the transaction is commited
       
       By default JPA does not normally write changes to the database until the transaction is committed. This is normally desirable as it avoids database access, resources and locks 
       until required. It also allows database writes to be ordered, and batched for optimal database access, and to maintain integrity constraints and avoid deadlocks. This means that 
       when you call persist, merge, or remove the database DML INSERT, UPDATE, DELETE is not executed, until commit, or until a flush is triggered
       
         */
	}

	@Override
	public void deleteArticle(int articleId) {
		entityManager.remove(getArticleById(articleId));

	}

	@Override
	public boolean articleExists(String title, String location) {
		String hql = "FROM Article where title=? and locaton=?";
		int count = entityManager.createQuery(hql).setParameter(1, title)
				                                  .setParameter(2, location)
				                                  .getResultList().size();
		return count >0 ?true:false;
	}

}
