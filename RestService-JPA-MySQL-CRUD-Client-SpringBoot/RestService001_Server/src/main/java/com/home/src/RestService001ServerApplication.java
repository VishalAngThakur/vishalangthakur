package com.home.src;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestService001ServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestService001ServerApplication.class, args);
	}
}
