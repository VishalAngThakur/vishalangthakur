package com.home.src.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.home.src.entity.Article;
import com.home.src.exception.ArticleNotFoundException;
import com.home.src.service.IArticleService;

/**
 * ---------------------------------------------------------
 * HTTP Methods definition
 * ---------------------------------------------------------
 * GET - For SELECT and FETCH Operations
 * PUT - For Update Operations
 * POST - Not well defined but for UPDTAE and ADD Operations
 * DELETE - Remove Operation
 * ---------------------------------------------------------- 
 * @author NisVis
 *
 */

@RestController
@RequestMapping(value="/api")
public class ArticleController {
   
	@Autowired
	private IArticleService articleService;
	
	@RequestMapping(value="/articles", method=RequestMethod.GET)
	public ResponseEntity<List<Article>> getAllArticles()
	{
		return new ResponseEntity(articleService.getAllArticles(), HttpStatus.OK);
	}

	@RequestMapping(value="/articles/{id}", method=RequestMethod.GET)
	public ResponseEntity<Article> getArticleByID(@PathVariable int id)
	{
		Article articleOpObj =  articleService.getArticleById(id);
		if(null == articleOpObj)
		{
			throw new ArticleNotFoundException(id);
		}
		return new ResponseEntity(articleOpObj,HttpStatus.OK);
	}
	
	@PostMapping("articles")
	public ResponseEntity<?> addArticle(@RequestBody Article article, UriComponentsBuilder builder) {
                boolean flag = articleService.addArticle(article);
                if (flag == false) {
        	    return new ResponseEntity<Void>(HttpStatus.CONFLICT);
                }
                HttpHeaders headers = new HttpHeaders();
                headers.setLocation(builder.path("/article/{id}").buildAndExpand(article.getArticleId()).toUri());
                return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	@PutMapping("articles")
	public ResponseEntity<Article> updateArticle(@RequestBody Article article) {
		Article articleOpObj =  articleService.getArticleById(article.getArticleId());
		if(null == articleOpObj)
		{
			throw new ArticleNotFoundException(article.getArticleId());
		}
		articleService.updateArticle(article);
		return new ResponseEntity<Article>(article, HttpStatus.OK);
	}
	
	@DeleteMapping("articles/{id}")
	public ResponseEntity<?> deleteArticle(@PathVariable("id") Integer id) {
		
		Article articleOpObj =  articleService.getArticleById(id);
		if(null == articleOpObj)
		{
			throw new ArticleNotFoundException(id);
		}
		articleService.deleteArticle(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
}
