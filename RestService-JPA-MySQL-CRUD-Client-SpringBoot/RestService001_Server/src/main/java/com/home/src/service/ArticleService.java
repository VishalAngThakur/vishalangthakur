package com.home.src.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.home.src.dao.IArticleDao;
import com.home.src.entity.Article;


@Service
public class ArticleService implements IArticleService{

	@Autowired
	private IArticleDao articleDao;
	
	@Override
	public List<Article> getAllArticles() {
		return articleDao.getAllArticles();
	}

	@Override
	public Article getArticleById(int articleId) {
		return articleDao.getArticleById(articleId);
	}

	@Override
	public boolean addArticle(Article article) {
		if(articleDao.articleExists(article.getTitle(), article.getCategory()))
		{
			return false;
		}
		articleDao.addArticle(article);
		return true;
	}

	@Override
	public void updateArticle(Article article) {
        articleDao.updateArticle(article);		
	}

	@Override
	public void deleteArticle(int articleId) {
		articleDao.deleteArticle(articleId);
	}

}
