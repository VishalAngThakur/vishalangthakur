package com.home.src;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.home.src.client.service.RestClientUtility;

@SpringBootApplication
public class RestService001ClientApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(RestService001ClientApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		System.out.println("Inside Command Line Runner");
		RestClientUtility util = new RestClientUtility();
		//util.getAllArticlesDemo();
        util.getArticleByIdDemo();
    	//util.addArticleDemo();
    	//util.updateArticleDemo();
    	//util.deleteArticleDemo();		
        
        System.out.println("I am done...");
	}
}
