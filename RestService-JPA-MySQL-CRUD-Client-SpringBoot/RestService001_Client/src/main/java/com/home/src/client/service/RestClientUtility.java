package com.home.src.client.service;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.home.src.entity.Article;


public class RestClientUtility {
	
	public static void main(String[] args) {
	
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/api/articles**({42";
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<Article> responseEntity = null;
		try {
		responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
				Article.class, 14);
		}catch(HttpClientErrorException ex)
		{
			if(ex.getStatusCode() == HttpStatus.NOT_FOUND) {
			     	System.out.println(ex.getResponseBodyAsString());			
			}
			throw ex;
		}
		Article article = responseEntity.getBody();
		System.out.println("Id:" + article.getArticleId() + ", Title:" + article.getTitle() + ", Category:"
				+ article.getCategory());
		
	}
	
	
	public void getArticleByIdDemo() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/api/articles/{id}";
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<Article> responseEntity = null;
		try {
		responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
				Article.class, 14);
		}catch(HttpClientErrorException ex)
		{
			if(ex.getStatusCode() == HttpStatus.NOT_FOUND) {
			     	System.out.println(ex.getResponseBodyAsString());			
			}
			throw ex;
		}
		Article article = responseEntity.getBody();
		System.out.println("Id:" + article.getArticleId() + ", Title:" + article.getTitle() + ", Category:"
				+ article.getCategory());
	}

	public void getAllArticlesDemo() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/api/articles";
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<Article[]> responseEntity = null;
		try {
		responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
				Article[].class, 14);
		}catch(HttpClientErrorException ex)
		{
			if(ex.getStatusCode() == HttpStatus.NOT_FOUND) {
			     	System.out.println(ex.getResponseBodyAsString());			
			}
			throw ex;
		}
		Article[] articles = responseEntity.getBody();
		for (Article article : articles) {
			System.out.println("Id:" + article.getArticleId() + ", Title:" + article.getTitle() + ", Category: "
					+ article.getCategory());
		}
	}

	public void addArticleDemo() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/api/articles";
		Article objArticle = new Article();
		objArticle.setTitle("Spring REST Security using Hibernate");
		objArticle.setCategory("Spring");
		HttpEntity<Article> requestEntity = new HttpEntity<Article>(objArticle, headers);
		URI uri = null;
		try {
			 uri = restTemplate.postForLocation(url, requestEntity);
		}catch(HttpClientErrorException ex)
		{
			if(ex.getStatusCode() == HttpStatus.CONFLICT) {
			     	System.out.println(ex.getResponseBodyAsString());			
			}
			throw ex;
		}
		System.out.println(uri.getPath());
	}

	public void updateArticleDemo() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/api/articles";
		Article objArticle = new Article();
		objArticle.setArticleId(1);
		objArticle.setTitle("Update:Java Concurrency");
		objArticle.setCategory("Java");
		HttpEntity<Article> requestEntity = new HttpEntity<Article>(objArticle, headers);
		try {
			restTemplate.put(url, requestEntity);
		}catch(HttpClientErrorException ex)
		{
			if(ex.getStatusCode() == HttpStatus.NOT_FOUND) {
			     	System.out.println(ex.getResponseBodyAsString());			
			}
			throw ex;
		}
	}

	public void deleteArticleDemo() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/api/articles/{id}";
		HttpEntity<Article> requestEntity = new HttpEntity<Article>(headers);
		try {
			restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, 4);
		}catch(HttpClientErrorException ex)
		{
			if(ex.getStatusCode() == HttpStatus.NOT_FOUND) {
			     	System.out.println(ex.getResponseBodyAsString());			
			}
			throw ex;
		}
	}
}
