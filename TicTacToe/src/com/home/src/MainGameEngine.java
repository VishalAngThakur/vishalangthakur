package com.home.src;

public class MainGameEngine {
	public static void main(String[] args) {

		System.err.println("\n\t\tWelcome to the fun world of Tic-Tac-Toe!!!");
		System.err.println("\n\t\t------------------------------------------");
		BoardGame game = new BoardGame();
		game.initializeGame();
		game.displayBoard();
		System.err.println("\n\n\t\t Let's Play \n");
		while(!game.resultAvailable)
		{
			game.askForMove();
			game.displayBoard();
		}
		
	}

}
