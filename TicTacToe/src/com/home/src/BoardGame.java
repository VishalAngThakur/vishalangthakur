package com.home.src;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 
 * @author NisVis
 * Home project
 *
 */
public class BoardGame {

	private final String[][] board;
	private final Scanner sc = new Scanner(System.in);
	private int counter = 1;
	private Map<Integer, int []> pendingMoves = new HashMap<>(9);
	private Player playerInAction;
	public boolean resultAvailable = false;

	public BoardGame() {
		super();
		this.board = new String[3][3];
	}

	public void initializeGame() {
		int index = 1;
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				board[row][col] = " " + index;
				int [] boardIndex = {row, col};
				pendingMoves.put(index, boardIndex);
				index++;
			}
		}
	}

	public void displayBoard() {
		System.out.println("\n");
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				System.out.print(board[row][col]);
				if (col < 2)
					System.out.print("\t|");
			}
			if (row < 2)
				System.out.println("\n-----------------------------");
		}

	}

	public void askForMove() {
		if (counter % 2 == 0)
			playerInAction = Player.X;
		else
			playerInAction = Player.O;
		System.out.println("\n\n"+ playerInAction.toString()+", Please make your move[e.g. 1-9]");

		boolean validMove = false;

		while (!validMove) {
			int moveIndex = sc.nextInt();
			validMove = validateMove(moveIndex, playerInAction);
			lookForWinnerOrTie();
		}
		counter++;

	}

	private void lookForWinnerOrTie() {

		for (int row = 0; row < board.length; row++) {
			if(board[row][0]==board[row][1] && board[row][1] == board[row][2])
			{
				resultAvailable = true;
				System.err.println(board[row][0]+ " Wins, Celebration!!!!");
			}
		}

		for (int col = 0; col < board.length; col++) {
			if(board[0][col]==board[1][col] && board[1][col] == board[2][col])
			{
				resultAvailable = true;
				System.err.println(board[0][col]+ " Wins, Celebration!!!!");
			}
		}
		
		if(
		   (board[0][0].equals(board[1][1]) && board[1][1].equals(board[2][2]))
			||
		   (board[0][2].equals(board[1][1]) && board[1][1].equals(board[2][0]))
			)
		{
			resultAvailable = true;
			System.err.println(board[1][1]+ " Wins, Celebration!!!!");
		}
		
		if(pendingMoves.size()==0 && !resultAvailable)
		{
			resultAvailable = true;
			System.err.println("It's a TIE!!!!");
		}
		
		if(resultAvailable)
		{
			displayBoard();
			System.out.println();
			gameShutDown();
		}
		
	}

	public void gameShutDown() {
        System.out.println("Shutting Game Down!!!");
        System.exit(-1);
	}

	private boolean validateMove(int moveIndex, Player player) {
		if(pendingMoves.containsKey(moveIndex)){
			int [] bucketToUpdate = pendingMoves.get(moveIndex);
			board[bucketToUpdate[0]][bucketToUpdate[1]]=player.toString();
		    pendingMoves.remove(moveIndex);
			return true;
		}
		else{
    	    System.out.println("\nInvalid move, try again!!!");
			return false;
		}
	}

}
