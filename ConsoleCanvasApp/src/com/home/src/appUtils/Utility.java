package com.home.src.appUtils;

import com.home.src.core.Canvas;

/**
 * <info> Utility helper class for the app
 * @author NisVis
 *
 */
public class Utility {
	
	/**
	 * Method to validate if input is a valid number or not
	 * @param arg
	 *          Accepts String
	 * @return 
	 *      True, if valid number
	 */
	public static boolean isNumeric(String arg) {
		return arg != null && arg.matches("^[+]?\\d+$");
	}

	/**
	 * 
	 * @param canvas
	 *          Main canvas object with reference to pixel where print is happening 
	 * @param x1 
	 *          Start coordinates for pixel
	 * @param y1
	 *          Start coordinates for pixel
	 * @param x2
	 *          End coordinates for pixel
	 * @param y2
	 *          End coordinates for pixel
	 * @param printChar
	 *          Character to print
	 */
	public static void updatePixel(Canvas canvas, int x1, int y1, int x2, int y2, char printChar) {
		for (int row = y1; row <= y2; row++) {
			for (int col = x1; col <= x2; col++) {
				canvas.getPixel()[row][col] = printChar;
			}
		}
	}
}
