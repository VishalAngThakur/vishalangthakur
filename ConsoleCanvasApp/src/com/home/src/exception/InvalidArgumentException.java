package com.home.src.exception;
/**
 * Exception to represent Invalid argument from Users
 * @author Vis
 *
 */
public class InvalidArgumentException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidArgumentException(String message) {
		super(message);
	}

}
