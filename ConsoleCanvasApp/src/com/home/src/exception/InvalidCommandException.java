package com.home.src.exception;
/**
 * Exception to represent command syntactically failing
 * @author Vis
 *
 */
public class InvalidCommandException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidCommandException(String message) {
		super(message);
	}

}
