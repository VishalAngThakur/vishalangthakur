package com.home.src.exception;

/**
 * Exception to represent shape paint activity when canvas.pixel is not initialized
 * @author Vis
 *
 */
public class CanvasNotInitializedException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CanvasNotInitializedException(String message) {
		super(message);
	}

}
