package com.home.src.factory;

import com.home.src.core.BucketFill;
import com.home.src.core.Canvas;
import com.home.src.core.Line;
import com.home.src.core.Rectangle;
import com.home.src.core.Shape;
import com.home.src.core.UserCommand;

/**
 * <info> Factory of Shape objects
 * @author Vis
 *
 */
public class ShapeFactory {
	private Canvas canvas;
	private UserCommand usrCmd;
	
	/**
	 * Method to construct shape factory object
	 * @param canvas
	 * @param usrCmd
	 */
	public ShapeFactory(Canvas canvas, UserCommand usrCmd) {
		super();
		this.canvas = canvas;
		this.usrCmd = usrCmd;
	}
	
	/**
	 * Factory method to return shape object
	 * @param usrCmd
	 *          Instance of UserCommand 
	 * @return
	 *        shape object
	 */
	public Shape getShape(UserCommand usrCmd)
	{
		Shape shapeObj = null;
		String [] cmd = usrCmd.getCommand().split(" ");
		char shapeType = cmd[0].charAt(0);
		switch(shapeType){
		case 'L':
			shapeObj = new Line(canvas, usrCmd, 'x');
			break;
		case 'R':
			shapeObj = new Rectangle(canvas, usrCmd, 'x');
			break;
		case 'B':
			shapeObj = new BucketFill(canvas, usrCmd, cmd[3].charAt(0));
			break;
		}
		return shapeObj;
	}

}
