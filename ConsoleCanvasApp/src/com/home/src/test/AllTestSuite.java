package com.home.src.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    UserCommandTest.class,  
    UtilityTest.class,      
    ShapeFactoryTest.class,
    LineTest.class,
    RectangleTest.class,
    BucketFillTest.class
})
public class AllTestSuite {
}