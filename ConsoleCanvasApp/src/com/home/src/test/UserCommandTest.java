package com.home.src.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.home.src.core.UserCommand;
import com.home.src.exception.InvalidCommandException;

public class UserCommandTest {

	@Test
	public void testValidateCommand_validInput_Q() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		assertTrue(command.validateCommand("Q"));
	}
	
	@Test
	public void testValidateCommand_validInput_C() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		assertTrue(command.validateCommand("C 34 10"));
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testValidateCommand_inValidInput_C() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		command.validateCommand("C 34");
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testValidateCommand_inValidInput_C_ExtraParam() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		command.validateCommand("C 34");
	}
	
	@Test
	public void testValidateCommand_validInput_L() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		assertTrue(command.validateCommand("L 6 1 6 2"));
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testValidateCommand_inValidInput_L() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		command.validateCommand("L 34 ");
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testValidateCommand_inValidInput_L_ExtraParam() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		command.validateCommand("L 6 1 6 2 5");
	}
	
	@Test
	public void testValidateCommand_validInput_B() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		assertTrue(command.validateCommand("B 6 1 o"));
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testValidateCommand_inValidInput_B() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		command.validateCommand("B 34 ");
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testValidateCommand_inValidInput_B_ExtraParam() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		command.validateCommand("L 6 1 d 3");
	}
	
	@Test
	public void testValidateCommand_validInput_R() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		assertTrue(command.validateCommand("R 6 1 2 5"));
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testValidateCommand_inValidInput_R() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		command.validateCommand("R 34 ");
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testValidateCommand_inValidInput_R_ExtraParam() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		command.validateCommand("R 6 1 d 3 9");
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testValidateCommand_inValidInput_UnknownCommand() throws InvalidCommandException {
		UserCommand command = new UserCommand();
		command.validateCommand("D 34 ");
	}

}
