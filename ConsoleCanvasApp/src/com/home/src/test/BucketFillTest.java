package com.home.src.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.home.src.core.Canvas;
import com.home.src.core.BucketFill;
import com.home.src.core.UserCommand;
import com.home.src.exception.CanvasNotInitializedException;
import com.home.src.exception.InvalidArgumentException;

public class BucketFillTest {
	
	private Canvas canvas;
	private UserCommand usrCmd;

	@Test
	public void testValidate_validInput() throws CanvasNotInitializedException, InvalidArgumentException {
		canvas = new Canvas(); 
		canvas.setBoundaries(22, 4);
		canvas.setPixel(new char[canvas.getHeight()][canvas.getWeight()]);
		usrCmd = new UserCommand("B 6 1 0");
		BucketFill bucketFillObj = new BucketFill(canvas, usrCmd,'x');
		assertTrue(bucketFillObj.validate());
	}
	
	@Test(expected=InvalidArgumentException.class)
	public void testValidate_inValidInput_OutOfPixelBound() throws CanvasNotInitializedException, InvalidArgumentException {
		canvas = new Canvas(); 
		canvas.setBoundaries(22, 4);
		canvas.setPixel(new char[canvas.getHeight()][canvas.getWeight()]);
		usrCmd = new UserCommand("B 50 1 50 90");
		BucketFill bucketFillObj = new BucketFill(canvas, usrCmd,'x');
		bucketFillObj.validate();
	}
	
	@Test(expected=CanvasNotInitializedException.class)
	public void testValidate_validInput_canvasPixelNotInitialized() throws CanvasNotInitializedException, InvalidArgumentException {
		canvas = new Canvas(); 
		canvas.setBoundaries(22, 4);
		usrCmd = new UserCommand("B 6 1 o");
		BucketFill bucketFillObj = new BucketFill(canvas, usrCmd,'x');
		bucketFillObj.validate();
	}

}
