package com.home.src.test;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.home.src.core.BucketFill;
import com.home.src.core.Canvas;
import com.home.src.core.Line;
import com.home.src.core.Rectangle;
import com.home.src.core.Shape;
import com.home.src.core.UserCommand;
import com.home.src.factory.ShapeFactory;

public class ShapeFactoryTest {

	private static ShapeFactory shapeFactory;
	private static Canvas canvas;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		canvas = new Canvas();
		canvas.setBoundaries(22, 4);
		canvas.setPixel(new char[canvas.getHeight()][canvas.getWeight()]);
	}

	@Test
	public void testShapeFactory_ValidInput_L() {
	    UserCommand usrCmd = new UserCommand("L 20 4 3 7");
	    shapeFactory = new ShapeFactory(canvas, usrCmd);
		Shape shapeObj = shapeFactory.getShape(usrCmd);
		assertTrue(shapeObj instanceof Line);
	}
	
	@Test
	public void testShapeFactory_ValidInput_R() {
		UserCommand usrCmd = new UserCommand("R 20 4 3 7");
		shapeFactory = new ShapeFactory(canvas, usrCmd);
		Shape shapeObj = shapeFactory.getShape(usrCmd);
		assertTrue(shapeObj instanceof Rectangle);
	}
	
	@Test
	public void testShapeFactory_ValidInput_B() {
		canvas.setPixel(new char[canvas.getHeight()][canvas.getWeight()]);
		UserCommand usrCmd = new UserCommand("B 20 4 3 7");
		shapeFactory = new ShapeFactory(canvas, usrCmd);
		Shape shapeObj = shapeFactory.getShape(usrCmd);
		assertTrue(shapeObj instanceof BucketFill);
	}

}
