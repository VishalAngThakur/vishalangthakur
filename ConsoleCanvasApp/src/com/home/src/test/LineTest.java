package com.home.src.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.home.src.core.Canvas;
import com.home.src.core.Line;
import com.home.src.core.UserCommand;
import com.home.src.exception.CanvasNotInitializedException;
import com.home.src.exception.InvalidArgumentException;

public class LineTest {
	
	private Canvas canvas;
	private UserCommand usrCmd;

	@Test
	public void testValidate_validInput() throws CanvasNotInitializedException, InvalidArgumentException {
		canvas = new Canvas(); 
		canvas.setBoundaries(22, 4);
		canvas.setPixel(new char[canvas.getHeight()][canvas.getWeight()]);
		usrCmd = new UserCommand("L 6 1 6 2");
		Line lineObj = new Line(canvas, usrCmd,'x');
		assertTrue(lineObj.validate());
	}
	
	@Test(expected=InvalidArgumentException.class)
	public void testValidate_inValidInput() throws CanvasNotInitializedException, InvalidArgumentException {
		canvas = new Canvas(); 
		canvas.setBoundaries(22, 4);
		canvas.setPixel(new char[canvas.getHeight()][canvas.getWeight()]);
		usrCmd = new UserCommand("L 1 1 6 2");
		Line lineObj = new Line(canvas, usrCmd,'x');
		lineObj.validate();
	}
	
	@Test(expected=InvalidArgumentException.class)
	public void testValidate_inValidInput_OutOfPixelBound() throws CanvasNotInitializedException, InvalidArgumentException {
		canvas = new Canvas(); 
		canvas.setBoundaries(22, 4);
		canvas.setPixel(new char[canvas.getHeight()][canvas.getWeight()]);
		usrCmd = new UserCommand("L 50 1 50 90");
		Line lineObj = new Line(canvas, usrCmd,'x');
		lineObj.validate();
	}
	
	@Test(expected=CanvasNotInitializedException.class)
	public void testValidate_validInput_canvasPixelNotInitialized() throws CanvasNotInitializedException, InvalidArgumentException {
		canvas = new Canvas(); 
		canvas.setBoundaries(22, 4);
		usrCmd = new UserCommand("L 6 1 6 2");
		Line lineObj = new Line(canvas, usrCmd,'x');
		lineObj.validate();
	}

}
