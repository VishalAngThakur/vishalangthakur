package com.home.src.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.home.src.appUtils.Utility;
import com.home.src.core.Canvas;

public class UtilityTest {

	private Canvas canvas;

	@Before
	public void setUp() throws Exception {
		canvas = new Canvas();
		canvas.setBoundaries(22, 4);
		canvas.setPixel(new char[canvas.getHeight()][canvas.getWeight()]);
	}

	@Test
	public void testY()
	{
		assertTrue(true);
	}
	
	@Test
	public void testIsNumeric_inputPositiveInteger() {
    assertTrue(Utility.isNumeric("2"));
	}

	@Test
	public void testIsNumeric_inputPositiveIntegerWithPlusSign() {
		assertTrue(Utility.isNumeric("+2"));
	}
	
	@Test
	public void testIsNumeric_inputPositiveIntegerSurroundedWithPlusSign() {
		assertFalse(Utility.isNumeric("+2+"));
	}
	
	@Test
	public void testIsNumeric_inputNegativeInteger() {
		assertFalse(Utility.isNumeric("-2"));
	}
	
	@Test
	public void testIsNumeric_inputDecimal() {
		assertFalse(Utility.isNumeric("2.2"));
	}
	
	@Test
	public void testIsNumeric_inputChar() {
		assertFalse(Utility.isNumeric("a"));
	}
	
	@Test
	public void testIsNumeric_inputSpecialChar() {
		assertFalse(Utility.isNumeric("*"));
	}
	
	@Test
	public void testIsNumeric_inputBlank() {
		assertFalse(Utility.isNumeric(" "));
	}
	
	@Test
	public void testIsNumeric_inputNull() {
		assertFalse(Utility.isNumeric(null));
	}

	@Test
	public void testDrawLine() {
		Utility.updatePixel(canvas, 6, 1, 6, 3, 'X');
		System.out.println(canvas.getPixel()[1][6]);
		assertTrue(canvas.getPixel()[1][6] == 'X');
		assertTrue(canvas.getPixel()[2][6] == 'X');
		assertTrue(canvas.getPixel()[3][6] == 'X');
		assertTrue((int)canvas.getPixel()[1][5] == 0 );
	}

}
