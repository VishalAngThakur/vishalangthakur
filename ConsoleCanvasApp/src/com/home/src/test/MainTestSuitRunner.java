package com.home.src.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
/**
 * Main class to trigger all test cases
 * @author admin
 *
 */
public class MainTestSuitRunner {

	 public static void main(String[] args) {
	      Result result = JUnitCore.runClasses(AllTestSuite.class);

	      for (Failure failure : result.getFailures()) {
	         System.out.println(failure.toString());
	      }
	   }
}