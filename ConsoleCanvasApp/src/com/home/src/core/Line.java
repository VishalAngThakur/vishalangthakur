package com.home.src.core;

import com.home.src.exception.CanvasNotInitializedException;
import com.home.src.exception.InvalidArgumentException;

/**
 * Class representing Line, the command for drawing line
 * @author Vis
 *
 */
public class Line implements Shape {

	private Canvas canvas;
	private UserCommand usrCmd;
	private int x1;
	private int y1;
	private int x2;
	private int y2;
	private char printChar;
	
	/**
	 * Method to construct Line Object
	 * 
	 * @param canvas
	 *           Canvas object
	 * @param usrCmd
	 *           User command  
	 * @param printChar
	 *           character to print
	 */
	public Line(Canvas canvas, UserCommand usrCmd, char printChar) {
		super();
		this.canvas = canvas;
		this.usrCmd = usrCmd;
		String [] cmd = this.usrCmd.getCommand().split(" ");
		this.x1 = Integer.parseInt(cmd[1]);
		this.y1 = Integer.parseInt(cmd[2]);
		this.x2 = Integer.parseInt(cmd[3]);
		this.y2 = Integer.parseInt(cmd[4]);
		this.printChar = printChar;
	}

	/**
	 * Method to update canvas.pixel array with user entered coordinates
	 */
	@Override
	public void paintPixel() {

		for (int row = y1; row <=y2; row++) {
			for (int col = x1; col <=x2; col++) {
				canvas.getPixel()[row][col] = this.printChar;
			}
		}
        canvas.addCommand(usrCmd);
	}

	/**
	 * Method to validate user entered coordinates
	 */
	@Override
	public boolean validate() throws CanvasNotInitializedException, InvalidArgumentException {
		// TODO Auto-generated method stub
		boolean validUsrInput = true;
		if(null == canvas.getPixel())
		{
			validUsrInput = false;
			throw new CanvasNotInitializedException("Canvas pixels are not initialized!!!");
		}
		if(!(x1 == x2 || y1 == y2))
		{
			validUsrInput = false;
			throw new InvalidArgumentException("Invalid Argument..Only horizontal and Vertical lines are supported!!!");
		}
		
		if((x1<0 || x1 > canvas.getWeight()-1) ||(y1 < 0 || y1 > canvas.getHeight()-1)
				|| (x2<0 || x2 > canvas.getWeight()-1) ||(y2 < 0 || y2 > canvas.getHeight()-1))
		{
			validUsrInput = false;
			throw new InvalidArgumentException("Invalid Argument..Cordinates provided is not available within canvas!!!");
		}
		
		return validUsrInput;
	}

	public char getPrintChar() {
		return printChar;
	}

	public void setPrintChar(char printChar) {
		this.printChar = printChar;
	}

	
	
}
