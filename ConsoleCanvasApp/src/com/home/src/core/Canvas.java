package com.home.src.core;

import java.util.LinkedList;
import java.util.List;

/**
 * Class representing canvas, the page for drawing
 * @author Vis
 *
 */
public class Canvas implements BusinessObject {
	
	private char[][] pixel; // The core array which represents pixel of any paint program
	private int height;
	private int weight;
	private List<UserCommand> usrCmdLst; // usrCmslist can be used in case we ever want to implement undo/redo functionality

	/**
	 * constructing Canvas object
	 */
	public Canvas() {
		super();
		this.usrCmdLst = new LinkedList<>();
	}

	/**
	 * validate user input
	 */
	@Override
	public boolean validate() {
		//No additional validation needed at this point
		return true;
	}

	/**
	 * Method to construct array representing canvas and populating with value
	 */
	public void paintScreen() {
		for (int row = 0; row < pixel.length; row++) {
			for (int col = 0; col < pixel[row].length; col++) {
				if (row == 0 || row == height - 1) {
					pixel[row][col] = '-';
					System.out.print("-");
				} else if ((col == 0 && (row != 0 && row != pixel.length - 1))
						|| (col == pixel[row].length - 1 && (row != 0 && row != pixel.length - 1))) {
					pixel[row][col] = '|';
					System.out.print("|");
				} else {
					System.out.print(pixel[row][col]);
				}
			}
			System.out.println();
		}
	}

	/**
	 * Method to record all the valid commands from users
	 * @param usrCommand
	 * @return
	 */
	public boolean addCommand(UserCommand usrCommand) {
		usrCmdLst.add(usrCommand);
		return true;
	}

	/**
	 * Method to set boundary coordinates for pixel array
	 * @param weight
	 * @param height
	 */
	public void setBoundaries(int weight, int height) {
		this.weight = weight;
		this.height = height;
	}

	/**
	 * Getter for canvas.pixel array
	 * @return
	 */
	public char[][] getPixel() {
		return pixel;
	}

	/**
	 * Setter for canvas.pixel array
	 * @return
	 */
	public void setPixel(char[][] pixel) {
		this.pixel = pixel;
	}

	public int getHeight() {
		return height;
	}

	public int getWeight() {
		return weight;
	}

	/**
	 * Getter for all successful command made
	 * @return
	 */
	public List<UserCommand> getUsrCmdLst() {
		return usrCmdLst;
	}

}
