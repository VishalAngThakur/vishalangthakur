package com.home.src.core;

/**
 * <info> Root interface for all paint/shape tools like Line, rectangle etc.
 * @author Vis
 *
 */
public interface Shape extends BusinessObject {
	public void paintPixel();
}
