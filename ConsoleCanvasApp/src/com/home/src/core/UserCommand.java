package com.home.src.core;

import com.home.src.appUtils.Utility;
import com.home.src.exception.InvalidCommandException;

/**
 * Class representing User entered commands
 * @author Vis
 *
 */
public class UserCommand {
	private String command;

	public UserCommand() {
		super();
	}

	public UserCommand(String command) {
		super();
		this.command = command;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	/**
	 * Method to validate user entered command
	 * 
	 * @param command
	 *        User entered command  
	 * @return
	 *        TRUE is command is valid
	 * @throws InvalidCommandException
	 *        When command is syntactically incorrect
	 */
	public boolean validateCommand(String command) throws InvalidCommandException {
		boolean validCmd = false;
		String[] arrCmd = command.split(" ");
		if (null != arrCmd && arrCmd[0].length() == 1 && Character.isLetter(arrCmd[0].charAt(0))) {
			char ch = command.charAt(0);
			switch (ch) {
			case 'Q':
				validCmd = true;
				break;
			case 'C':
				if (arrCmd.length == 3 && null != arrCmd[1] && null != arrCmd[2] && Utility.isNumeric(arrCmd[1])
						&& Utility.isNumeric(arrCmd[2])) {
					validCmd = true;
				}
				break;
			case 'L':
				if (arrCmd.length == 5 && null != arrCmd[1] && null != arrCmd[2] && null != arrCmd[3]
						&& null != arrCmd[4] && Utility.isNumeric(arrCmd[1]) && Utility.isNumeric(arrCmd[2])
						&& Utility.isNumeric(arrCmd[3]) && Utility.isNumeric(arrCmd[4])) {
					validCmd = true;
				}
				break;
			case 'R':
				if (arrCmd.length == 5 && null != arrCmd[1] && null != arrCmd[2] && null != arrCmd[3]
						&& null != arrCmd[4] && Utility.isNumeric(arrCmd[1]) && Utility.isNumeric(arrCmd[2])
						&& Utility.isNumeric(arrCmd[3]) && Utility.isNumeric(arrCmd[4])) {
					validCmd = true;
				}
				break;
			case 'B':
				if (arrCmd.length == 4 && null != arrCmd[1] && null != arrCmd[2] && null != arrCmd[3]
						&& Utility.isNumeric(arrCmd[1]) && Utility.isNumeric(arrCmd[2])
						&& (arrCmd[3].length() == 1 && Character.isLetter(arrCmd[3].charAt(0)))) {
					validCmd = true;
				}
				break;
			}
		}
		if (!validCmd) {
			throw new InvalidCommandException("Syntax error in Command");
		}
		return validCmd;
	}

}
