package com.home.src.core;

import com.home.src.exception.CanvasNotInitializedException;
import com.home.src.exception.InvalidArgumentException;

/**
 * Bucket Fill implementation of Shape interface
 * @author Vis
 *
 */
public class BucketFill implements Shape {

	private Canvas canvas;
	private UserCommand usrCmd;
	private int x;
	private int y;
	private char printChar;

	/**
	 * Constructing BucketFill object
	 * @param canvas
	 *          Canvas Object
	 * @param usrCmd
	 *          User Command Object 
	 * @param printChar
	 *          character to print
	 */
	public BucketFill(Canvas canvas, UserCommand usrCmd, char printChar) {
		super();
		this.canvas = canvas;
		this.usrCmd = usrCmd;

		String[] cmd = this.usrCmd.getCommand().split(" ");
		this.x = Integer.parseInt(cmd[1]);
		this.y = Integer.parseInt(cmd[2]);
		this.printChar = printChar;
	}

	@Override
	public void paintPixel() {
		// TODO Auto-generated method stub
		fillPaint(x, y, printChar);
		canvas.addCommand(usrCmd);

	}

	/**
	 * 
	 * @param x
	 *         row coordinate
	 * @param y
	 *          column coordinate
	 * @param charToPrint
	 *          char to print
	 */
	public void fillPaint(int x, int y, char charToPrint) {
		if (x > -1 && y > -1) {
			if ((int) canvas.getPixel()[y][x] != 0) {
				return;
			}
		}

		if (x > 0 || x < canvas.getPixel().length || y > 0 || y < canvas.getPixel()[0].length) {
			if ((int) canvas.getPixel()[y][x] == 0) {
				canvas.getPixel()[y][x] = charToPrint;
			}
			fillPaint(x + 1, y, charToPrint);
			fillPaint(x - 1, y, charToPrint);
			fillPaint(x, y - 1, charToPrint);
			fillPaint(x, y + 1, charToPrint);
		}
	}

	/**
	 * method to validate is canvas is initialized and argument is valid
	 */
	@Override
	public boolean validate() throws CanvasNotInitializedException, InvalidArgumentException {
		boolean validUsrInput = true;
		if (null == canvas.getPixel()) {
			validUsrInput = false;
			throw new CanvasNotInitializedException("Canvas pixels not initialized");
		}
		if ((x < 0 || x > canvas.getWeight() - 1) || (y < 0 || y > canvas.getHeight() - 1)) {
			validUsrInput = false;
			throw new InvalidArgumentException(
					"Invalid Argument..Cordinates provided is not available within canvas!!!");
		}
		return validUsrInput;
	}

	public char getPrintChar() {
		return printChar;
	}

	public void setPrintChar(char printChar) {
		this.printChar = printChar;
	}

}
