package com.home.src.core;

import com.home.src.exception.CanvasNotInitializedException;
import com.home.src.exception.InvalidArgumentException;

/**
 * Root interface for all core classes
 * @author Vis
 *
 */
public interface BusinessObject {
	public boolean validate() throws CanvasNotInitializedException, InvalidArgumentException;;
}
