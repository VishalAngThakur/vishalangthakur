package com.home.src.service;

import com.home.src.core.Canvas;
import com.home.src.core.UserCommand;

/**
 * class wrapping and abstracting request from users
 * @author Vis
 *
 */
public class InvokeCommandService {
	private Canvas canvas;
	private UserCommand usrCommand;

	public InvokeCommandService(Canvas canvas, UserCommand usrCommand) {
		super();
		this.canvas = canvas;
		this.usrCommand = usrCommand;
	}

	public void executeRequest()
	{
		char ch = this.usrCommand.getCommand().charAt(0);
		switch(ch){
		case 'C':
			new LoadCanvasImpl(canvas, usrCommand).execute();
			break;
		case 'Q':
			new ExitCanvasImpl().execute();
			break;
		default:
			new DrawShapeImpl(canvas, usrCommand).execute();
		}
	}
	
}
