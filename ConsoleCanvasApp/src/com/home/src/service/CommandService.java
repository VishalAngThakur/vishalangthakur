package com.home.src.service;

/**
 * Interface wrapping all activity post user command is entered
 * @author Vis
 *
 */
public interface CommandService {
 public void execute();
}
