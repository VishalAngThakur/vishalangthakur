package com.home.src.service;

import com.home.src.core.Canvas;
import com.home.src.core.Shape;
import com.home.src.core.UserCommand;
import com.home.src.exception.CanvasNotInitializedException;
import com.home.src.exception.InvalidArgumentException;
import com.home.src.factory.ShapeFactory;

/**
 * CommandService implementation for shape draw request
 * @author Vis
 *
 */
public class DrawShapeImpl implements CommandService{

	private Canvas canvas;
	private UserCommand usrCommand;
	
	public DrawShapeImpl(Canvas canvas, UserCommand usrCommand) {
		super();
		this.canvas = canvas;
		this.usrCommand = usrCommand;
	}



	@Override
	public void execute() {

		Shape shape = new ShapeFactory(canvas, usrCommand).getShape(usrCommand);
		boolean validShape = false;
		try{
			validShape = shape.validate();
		}catch(CanvasNotInitializedException|InvalidArgumentException e)
		{
			e.printStackTrace();
		}
		
		//Store command in canvas object
		if(validShape)
		{
			shape.paintPixel();
		}
		
		canvas.paintScreen();
	}

}
