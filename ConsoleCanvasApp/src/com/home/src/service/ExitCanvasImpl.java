package com.home.src.service;
/**
 * CommandService implementation for App close request
 * @author Vis
 *
 */
public class ExitCanvasImpl implements CommandService {

	@Override
	public void execute() {
		System.out.println("Shutting down...");
		System.exit(-1);
	}

}
