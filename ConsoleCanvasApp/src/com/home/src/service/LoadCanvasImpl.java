package com.home.src.service;

import com.home.src.core.Canvas;
import com.home.src.core.UserCommand;
/**
 * CommandService implementation for Load canvas and initialize canvas.pixel
 * @author Vis
 *
 */
public class LoadCanvasImpl implements CommandService {

	private Canvas canvas;
	private UserCommand usrCommand;
	
	public LoadCanvasImpl(Canvas canvas, UserCommand usrCommand) {
		super();
		this.canvas = canvas;
		this.usrCommand = usrCommand;
	}

	
	@Override
	public void execute() {

		String [] cmd = this.usrCommand.getCommand().split(" ");
		int weight = Integer.parseInt(cmd[1])+2;
		int height = Integer.parseInt(cmd[2])+2;
        canvas.setBoundaries(weight, height);	
        canvas.setPixel(new char[canvas.getHeight()][canvas.getWeight()]);
        canvas.paintScreen();
	}

}
