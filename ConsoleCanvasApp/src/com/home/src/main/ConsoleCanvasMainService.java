package com.home.src.main;

import java.util.Scanner;

import com.home.src.core.Canvas;
import com.home.src.core.UserCommand;
import com.home.src.exception.InvalidCommandException;
import com.home.src.service.InvokeCommandService;

/**
 * <info> Entry point of console canvas App
 * @author Vishal
 *
 */
public class ConsoleCanvasMainService {
	public static void main(String[] args) {
		Canvas canvas = new Canvas();
		Scanner scanner = new Scanner(System.in);
		UserCommand command = new UserCommand();
		
		//Clean all open resources during shutdown
		Runtime.getRuntime().addShutdownHook(new Thread(()->{
			scanner.close();
		}));

		while (true) {
			System.out.println("Enter Command : ");
			command = new UserCommand(scanner.nextLine());

			try {
				boolean validCommand = command.validateCommand(command.getCommand());
				if (validCommand) {
					InvokeCommandService brokerReq = new InvokeCommandService(canvas, command);
					brokerReq.executeRequest();
				}
			} catch (InvalidCommandException e) {
				e.printStackTrace();
			}
		}
	}
}
