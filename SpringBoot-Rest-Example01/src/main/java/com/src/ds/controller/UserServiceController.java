package com.src.ds.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.src.ds.entity.User;
import com.src.ds.service.UserService;

@RestController
public class UserServiceController {

	@Autowired
	private UserService service;
	
	@GetMapping(path="users")
	public List<User> showAllUsers()
	{
		return service.showAllUsers();
	}
	
	@GetMapping(path="/users/{name}")
	public User showUser(@PathVariable String name) {
		return service.showUser(name);
		
	}
	
}
