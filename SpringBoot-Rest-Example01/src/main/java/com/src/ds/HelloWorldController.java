package com.src.ds;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@GetMapping(path="/helloWorld")
	public String helloWorld() {
		return "Hello World";
	}
	
	@GetMapping(path="/greetings/{name}")
	public String greetings(@PathVariable String name)
	{
		return "Greeting, Master "+name;
	}
}
