package com.src.ds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestExample01Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestExample01Application.class, args);
	}
}
