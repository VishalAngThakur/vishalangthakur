package com.src.ds.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.src.ds.entity.User;

@Service
public class UserService {

	private static List<User> lstStudent;
	
	static {
		lstStudent = Arrays.asList(
				new User("Vishal",21,"CLSA"),
				new User("Roma",21,"Yahoo"),
				new User("Katy",19,"JPM")
				);
	}
	
	public List<User> showAllUsers()
	{
		return lstStudent;
	}
	
	public User showUser(@PathVariable String name) {
		return lstStudent.stream().filter(e -> name.equals(e.getName())).findFirst().get();
		
	}
	
}
